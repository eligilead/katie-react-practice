import express from "express";
// you need to import express

import getPosts from "../controllers/posts.js";

const router = express.Router();
// this creates the routes for the backend server

router.get("/", getPosts);

export default router;
// needs to be exported
