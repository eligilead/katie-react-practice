import express from "express";
// this is equivalent to -> const express = require('express')
// initialize in package.json
import bodyParser from "body-parser";
import mongoose from "mongoose";
import cors from "cors";
// import { MONGODB_USERNAME, MONGODB_PASSWORD } from ".env";

import postsRoutes from "./routes/posts.js";

const app = express();

app.use("/posts", postsRoutes);

app.use(bodyParser.json({ limit: "30mb", extended: true }));
app.use(bodyParser.urlencoded({ limit: "30mb", extended: true }));
app.use(cors());

const CONNECTION_URL =
  "mongodb+srv://elijahgilead:elijahgilead123@cluster0.86avw0s.mongodb.net/?retryWrites=true&w=majority";
//   const CONNECTION_URL = `mongodb+srv://${MONGODB_USERNAME}:${MONGODB_PASSWORD}@cluster0.86avw0s.mongodb.net/?retryWrites=true&w=majority`;
const PORT = process.env.PORT || 5000;

mongoose
  .connect(CONNECTION_URL)
  .then(() =>
    app.listen(PORT, () => console.log(`Server running on port: ${PORT}`))
  )
  .catch((error) => console.log(error.message));

// mongoose.set("useFindAndModify", false);
// this ensure no console messages show up
