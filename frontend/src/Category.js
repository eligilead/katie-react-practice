import {React, useState}  from "react";


function Category(props) {
    // in main budget store all the category and the amount for each category in state 
    // store values as well, maybe table
    // think of a dynamic table that place rows in table when you click button, you should see a catergory options and a iput for number when you save budget should change
    // and bew row will be seen
    // you can only click the button if the row is filled
    const [expense, setExpense] = useState({
      category: "Food",
      cost: 0,
    });
    const categoryList = [
      "Food",
      "Insurance",
      "Utilities",
      "Saving",
      "Transportation",
    ];
  
    const handleSubmit = (e) => {
      e.preventDefault();
      props.handleCategorySubmission(expense.category, expense.cost);
      // console.log("handle submit------>", expense);
      // console.log("handle submit------>", expense.category);
    };
  
    const handleChangeCategory = (e) => {
      const currentState = { ...expense };
      currentState.category = e.target.value;
      // console.log("test------>", currentState);
      setExpense(currentState);
    };
  
    const handleChangeCost = (e) => {
      const currentState = { ...expense };
      if (e.target.value === ''){
        currentState.cost = 0
        return currentState
      } else {
          currentState.cost = +e.target.value;
          // turns the string into a number e.target.value -> auto turns into a string,
          // also can't turn object into a string
          setExpense(currentState);
      }
      // console.log("handle cost e-target-value-->,", typeof e.target.value);
      console.log("handle cost currentState-->,", currentState);
  
      
    };
  
    return (
      <div>
        <section>
          <form onSubmit={handleSubmit}>
            <select onChange={(e) => handleChangeCategory(e)}>
              {categoryList.map((category, index) => {
                return <option key={index}>{category}</option>;
              })}
            </select>
            <input type="number" onChange={(e) => handleChangeCost(e)} />
            <input type="submit" />
          </form>
        </section>
        <br />
        {/* <section>
          <table>
            <tr>
              <th>Category</th>
              <th>Cost</th>
            </tr>
            <tbody>
              <tr>
                <td></td>
              </tr>
            </tbody>
          </table>
        </section> */}
      </div>
    );
  }

  export default Category;
