import React, { useState } from "react";
import Category from "./Category";

function BudgetForm() {
  const [budget, setBudget] = useState(0);
  const [userInput, setUserInput] = useState(0);
  const [expenses, setExpenses] = useState({});

  const handleSubmit = (e) => {
    e.preventDefault();
    setBudget(userInput);
    // console.log(e);
  };
  // map over expense object and render category for each expense
  const confirmExpense = (category, cost) => {
    const trackedExpense = { ...expenses };
    trackedExpense[category] = cost;
    setExpenses(trackedExpense);
    console.log("callback function final trackedExpense-->", trackedExpense);
    console.log("callback function final expense-->", expenses);
  };

  return (
    <div>
      <h2>{budget}</h2>
      <form onSubmit={handleSubmit}>
        <label>Budget: </label>
        <input
          onChange={(e) => setUserInput(e.target.value)}
          value={userInput}
          type="number"
        />
        <input type="submit" />
      </form>
      <p>this works</p>
      <Category handleCategorySubmission={confirmExpense} />

      <div>
        <table>
          <tr>
            <th>Category</th>
            <th>Cost</th>
          </tr>
          { Object.keys(expenses).map(function(key, value) {
            // I can also consider object.entries
            console.log(expenses)
            return(
          <tr>
            <td>{key}</td>
            <td>{expenses[key]}</td>
          </tr>
            )
          })}
        </table>
      </div>
    </div>
  );
}

export default BudgetForm;
